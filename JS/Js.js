function MostrarAuto(){

    alert("Mi autoevaluacion es 5");

}

async function MostrarNotas(){

    let url = new URLSearchParams(location.search);
    let codigo = (url.get("estudiante"));
    let notas = Number(url.get("notas"));
    if (notas === null){
        notas=0;
    }
   // let codigo= document.getElementById("codigo").value;
   // let notas= document.getElementById("nota").value;
    
    let ricardo= await buscarnotas(codigo);
    let ricarda=0;

    for (let j = 0; j < ricardo.estudiantes.length; j++) {
        if (codigo==ricardo.estudiantes[j].codigo){
            await informacion(ricardo,j,codigo);
            await eliminarNotas(ricardo,notas,j);
            ricarda=1;
        }
        
    }

    if (ricarda==0) {
       document.body.innerHTML=`<div class="container-fluid align-content-center text-center bg-danger p-2" >
       <h1> NO SE ENCONTRÓ NINGUN ESTUDIANTE CON ESTE CODIGO O NO INGRESÓ UN NUMERO VALIDO DE NOTAS A ELIMINAR</h1>
   </div>`;
    }

}

function buscarnotas(alumno) {
    salida = fetch("https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json")
    .then(res=> res.json() );
    
    return salida;
}

function eliminarNotas(json, num, codigo) {
    
    let notas1 = json.estudiantes[codigo].notas;
    console.log(notas1);
    let matrix = new Array(15);
    
    notas1.sort(function(a, b){ return a.valor > b.valor ? 1 : b.valor > a.valor ? -1 : 0 });

    for (let i = 0; i < notas1.length; i++) {
        matrix[i]= new Array(2); 
        matrix[i][0]=parseInt(notas1[i].id);
        matrix[i][1]=notas1[i].valor;
    }  
    
    for (let i = 0; i < num; i++) {
        matrix[i][1]=-1;
    }

    generarCharts(json,codigo,matrix);

    
}


function generarCharts(jason,codigo,matrix){

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Descripcion');
    data.addColumn('number', 'Valor');
    data.addColumn('string', 'Observacion');
    // crear las filas 
    let var1=0;
    let var2=0;
        

    for (let i = 0; i < jason.descripcion.length; i++) {
        data.addRows(1);
        data.setCell(i, 0, jason.descripcion[i].descripcion);
        data.setCell(i, 1, jason.estudiantes[codigo].notas[i].valor);
        if(matrix[i][1]==-1){
            data.setCell(i, 2, 'Nota eliminada');
        }else if(jason.estudiantes[codigo].notas[i].valor >= 3){
            data.setCell(i, 2, 'Nota aprobada');
            var1=var1+1;
        }else{
            data.setCell(i, 2, 'Nota reprobada');
            var2=var2+1;
        }
        
    }
    var table = new google.visualization.Table(document.getElementById('tabla'));
    table.draw(data, { showRowNumber: false, width: '90%', height: '100%' });

    pastel(var1,var2);
}

function informacion(json,codigo1,codi) {
    var data = new google.visualization.DataTable();
    // Add columns
    data.addColumn('string');
    data.addColumn('string');

    // Add empty rows
    data.addRows(3);
    data.setCell(0, 0, "Nombre: "); 
    data.setCell(0, 1, json.estudiantes[codigo1].nombre); 
    data.setCell(1, 0, "Código: "); 
    data.setCell(1, 1, codi); 
    data.setCell(2, 0, "Materia: "); 
    data.setCell(2, 1, json.nombreMateria); 

    var table = new google.visualization.Table(document.getElementById('info'));
    table.draw(data, { showRowNumber: false, width: '60%', height: '100%' });
}

function pastel(var1,var2){

    google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['items', 'examenes pasados'],
          ['notas aprovadas',     var1],
          ['Notas reprobadas',     var2]
        ]);

        var options = {
          title: 'Notas',
          is3D: true,

        };

        var chart = new google.visualization.PieChart(document.getElementById('tarta'));
        chart.draw(data, options);
      }


}


